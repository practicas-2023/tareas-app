import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { DouglasTareasComponent } from './components/douglas-tareas/douglas-tareas.component';
import { DanielTareasComponent } from './components/daniel-tareas/daniel-tareas.component';

@NgModule({
  declarations: [
    AppComponent,
    DouglasTareasComponent,
    DanielTareasComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
