import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DouglasTareasComponent } from './components/douglas-tareas/douglas-tareas.component';
import { DanielTareasComponent } from './components/daniel-tareas/daniel-tareas.component';

const routes: Routes = [
    { path: '', redirectTo: '', pathMatch: 'full'},
    { path: 'douglas', component: DouglasTareasComponent },
    { path: 'daniel', component: DanielTareasComponent },
    { path: '**', pathMatch: 'full', redirectTo: '' }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
