import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DanielTareasComponent } from './daniel-tareas.component';

describe('DanielTareasComponent', () => {
  let component: DanielTareasComponent;
  let fixture: ComponentFixture<DanielTareasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DanielTareasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DanielTareasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
