import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DouglasTareasComponent } from './douglas-tareas.component';

describe('DouglasTareasComponent', () => {
  let component: DouglasTareasComponent;
  let fixture: ComponentFixture<DouglasTareasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DouglasTareasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DouglasTareasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
